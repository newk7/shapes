package com.libertymutual.student.ian.programs.example01.shapes;

import java.awt.Color;
import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Triangle extends Shape {
	
	private final static Logger logger = LogManager.getLogger(Triangle.class);

    private int base;
	private int height;

    public Triangle(int base, int height, Color color) {
        super(color);
        this.base = base;
		this.height = height;
    }

    // provide a getArea implementation 
    @Override
    public BigDecimal getArea() {
		logger.info(Triangle.class + " Triangulating........ ");
        double area = 0.5*(base * height);
        return new BigDecimal(area);
    }
}

