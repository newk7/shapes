package com.libertymutual.student.ian.programs.example01.shapes;

import java.awt.Color;
import java.math.BigDecimal;

public class Rectangle extends Shape {

    private int side1Length;
    private int side2Length;

    public Rectangle(int side1, int side2, Color color) {
        super(color);
        this.side1Length = side1;
        this.side2Length = side2;
    }

    // provide a getArea implementation 
    @Override
    public BigDecimal getArea() {
        double area = side1Length * side2Length;
        return new BigDecimal(area);
    }
}

    
