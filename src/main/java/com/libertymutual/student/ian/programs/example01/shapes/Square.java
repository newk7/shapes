package com.libertymutual.student.ian.programs.example01.shapes;

import java.awt.Color;
import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Square extends Shape {
	
	private final static Logger logger = LogManager.getLogger(Square.class);

    private int sideLength;

    public Square(int side, Color color) {
        super(color);
        this.sideLength = side;
    }

    // provide a getArea implementation 
    @Override
    public BigDecimal getArea() {
		logger.info(Square.class + " Squares getting Squared.... ");
        double area = sideLength * sideLength;
        return new BigDecimal(area);
    }
}

    
