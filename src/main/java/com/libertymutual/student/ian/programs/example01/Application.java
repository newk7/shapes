package com.libertymutual.student.ian.programs.example01;

import java.awt.Color;
import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.libertymutual.student.ian.programs.example01.shapes.Shape;
import com.libertymutual.student.ian.programs.example01.shapes.Square;
import com.libertymutual.student.ian.programs.example01.shapes.Circle;
import com.libertymutual.student.ian.programs.example01.shapes.Rectangle;
import com.libertymutual.student.ian.programs.example01.shapes.Triangle;

public class Application {

	private final static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {

		logger.info(Application.class +" starting ");
        int radius = 10;
        Circle circle = new Circle(radius, Color.PINK);
        System.out.println(circle.getArea());

        int length = 100;
        Square square = new Square(length, Color.RED);
        System.out.println(square.getArea());

       int recLength1 = 20;
       int recLength2 = 30;
        Rectangle rectangle = new Rectangle(recLength1, recLength2, Color.GREEN);
        System.out.println(rectangle.getArea());
		System.out.println(rectangle.getColor());

		int base = 15;
		int height = 4;
		Triangle triangle = new Triangle(base, height, Color.BLUE);
		System.out.println(triangle.getArea());

		//System.exit(0);
    }
}
