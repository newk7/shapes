package com.libertymutual.student.ian.programs.example01.shapes;

import org.junit.Test;
import org.junit.Before;

import java.awt.Color;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class TriangleTest {
	
	Triangle testTriangle;

//	private Square square;

	@Before
	public void setupTriangle() {
		testTriangle = new Triangle(5,7,Color.blue);
	}

	@Test
	public void testGetArea() {
		BigDecimal area = testTriangle.getArea();
		BigDecimal expectedAnswer = new BigDecimal(17.5);
		assertEquals("Verify that area of triangle is correct", expectedAnswer, area);
	}

	@Test
	public void testGetColor() {
		Color color = testTriangle.getColor();
		Color expectedColor = Color.blue;
		assertEquals("verify that color is correct", expectedColor, color);
	}
}
