package com.libertymutual.student.ian.programs.example01.shapes;

import org.junit.Test;
import org.junit.Before;

import java.awt.Color;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class SquareTest {
	
	Square testSquare;

//	private Square square;

	@Before
	public void setupSquare() {
		testSquare = new Square(5,Color.blue);
	}

	@Test
	public void testGetArea() {
	//	Square square = new Square(5,Color.blue);
		BigDecimal area = testSquare.getArea();
		BigDecimal expectedAnswer = new BigDecimal(25);
		assertEquals("Verify that area of square is correct", expectedAnswer, area);
	}

	@Test
	public void testGetColor() {
		Color color = testSquare.getColor();
		Color expectedColor = Color.blue;
		assertEquals("verify that color is correct", expectedColor, color);
	}
}
