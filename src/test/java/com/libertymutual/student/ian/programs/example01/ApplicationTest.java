package com.libertymutual.student.ian.programs.example01;

import org.junit.Test;
import org.junit.Before;
import org.junit.Rule;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import static org.junit.Assert.assertEquals;

public class ApplicationTest{

	@Rule
	public final ExpectedSystemExit exit = ExpectedSystemExit.none();

	@Test
	public void testMain() {
        String[] args = new String[0];
		Application.main(args);
	}
}
