package com.libertymutual.student.ian.programs.example01.shapes;

import org.junit.Test;
import org.junit.Before;

import java.awt.Color;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class RectangleTest {
	
	Rectangle testRectangle;

//	private Rectangle rectangle;

	@Before
	public void setupRectangle() {
		testRectangle = new Rectangle(5, 12, Color.blue);
	}

//	@Test
//	public void testGetArea() {
//		BigDecimal area = testRectangle.getArea();
//		BigDecimal expectedAnswer = new BigDecimal(60);
//		assertEquals("Verify that area of Rectangle is correct", expectedAnswer, area);
//	}

	@Test
	public void testGetColor() {
		Color color = testRectangle.getColor();
		Color expectedColor = Color.blue;
		assertEquals("verify that color is correct", expectedColor, color);
	}
}
