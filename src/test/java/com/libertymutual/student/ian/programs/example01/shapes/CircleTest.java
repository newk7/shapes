package com.libertymutual.student.ian.programs.example01.shapes;

import org.junit.Test;
import org.junit.Before;

import java.awt.Color;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CircleTest {

	Circle testCircle;
	
	@Before 
	public void setupCircle() {
		testCircle = new Circle(10,Color.red);
	}

	@Test
	public void testGetArea() {
		BigDecimal area = testCircle.getArea();
		BigDecimal expectedAnswer = new BigDecimal(314);
		assertEquals("Verify that area is correct",expectedAnswer, area);
	}

	@Test
	public void testGetColor(){
		Color color = testCircle.getColor();
		Color expectedColor = Color.red;
		assertEquals("Verify that color is correct", expectedColor, color);
	}
		
}

